//
//  UIImage+toGrayScaleAddition.h
//  prototipoTabbed2
//
//  Created by Matias Castro on 29-07-13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage(addedFunctionalities)
-(UIImage *)convertToGrayScale;
-(NSData *)jpegRepresentationWithBytes:(NSInteger)_bytes
                           withSuccess:(void (^)(NSData *image))success;

-(UIImage *)fixOrientation;
- (UIImage *)imageScaledToSize:(CGSize)size;
@end
