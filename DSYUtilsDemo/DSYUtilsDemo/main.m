//
//  main.m
//  DSYUtilsDemo
//
//  Created by Matias Castro on 21-02-14.
//  Copyright (c) 2014 Dsarhoya. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
