//
//  DSYUtilsDemoTests.m
//  DSYUtilsDemoTests
//
//  Created by Matias Castro on 21-02-14.
//  Copyright (c) 2014 Dsarhoya. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface DSYUtilsDemoTests : XCTestCase

@end

@implementation DSYUtilsDemoTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
